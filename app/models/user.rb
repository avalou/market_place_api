class User < ApplicationRecord
  validates :email, uniqueness: true
  validates_format_of :email, with: /@/
  validates :password_digest, presence: true
  #validates :password_digest, length: { minimum: 6 }
  has_many :products, dependent: :destroy

  has_secure_password
end
