User.delete_all
User.reset_pk_sequence
Product.delete_all
Product.reset_pk_sequence

10.times do |i|
  user = User.create! email: "#{Faker::Name.first_name.downcase}@email.com", password: "azerty123"
  puts "Created user ##{i} - #{user.email}"

  2.times do |j|
    product = Product.create!(
      title: Faker::Commerce.product_name,
      price: rand(1.0..100.0),
      published: true,
      user_id: user.id
    )
    puts "Created a brand new product: #{product.title}"
  end
end